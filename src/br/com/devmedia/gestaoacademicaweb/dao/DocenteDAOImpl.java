package br.com.devmedia.gestaoacademicaweb.dao;

import java.util.List;

import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import br.com.devmedia.gestaoacademicaweb.model.Docente;

@Repository
public class DocenteDAOImpl implements DocenteDAO {

	@Autowired
	private SessionFactory sessionFactory;
	
	@Override
	public void adicionarDocente(Docente docente) {
		sessionFactory.getCurrentSession().save(docente);
	}

	@Override
	public List<Docente> listarDocentes() {
		return sessionFactory.getCurrentSession().createCriteria(Docente.class).list();
	}

	@Override
	public Docente verDocente(int id) {
		return (Docente) sessionFactory.getCurrentSession().load(Docente.class, new Integer(id));
	}

}